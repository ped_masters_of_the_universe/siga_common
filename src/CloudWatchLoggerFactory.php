<?php

namespace Avantis\Common;

use Aws\CloudWatchLogs\CloudWatchLogsClient;
use Maxbanton\Cwh\Handler\CloudWatch;
use Monolog\Logger;

class CloudWatchLoggerFactory
{
    /**
     * Create a custom Monolog instance.
     *
     * @param  array  $config
     * @return \Monolog\Logger
     */
    public function __invoke(array $config)
    {
        $sdkParams = [
            'region' => env('CLOUDWATCH_LOG_REGION'),
            'version' => env('CLOUDWATCH_LOG_VERSION'),
            'credentials' => [
                'key' => env('CLOUDWATCH_LOG_KEY'),
                'secret' => env('CLOUDWATCH_LOG_SECRET')
            ]
        ];

        // Instantiate AWS SDK CloudWatch Logs Client
        $client = new CloudWatchLogsClient($sdkParams);

        // Log group name, will be created if none
        $groupName = env('CLOUDWATCH_LOG_GROUP_NAME');

        // Log stream name, will be created if none
        $streamName = env('CLOUDWATCH_LOG_STREAM_NAME');

        // Days to keep logs, 14 by default. Set to `null` to allow indefinite retention.
        $retentionDays = env('CLOUDWATCH_LOG_RETENTION_DAYS');

        // Instantiate handler (tags are optional)
        $handler = new CloudWatch($client, $groupName, $streamName, $retentionDays, 10000, ['my-awesome-tag' => 'tag-value']);

        // Create a log channel
        $logger = new Logger(env('CLOUDWATCH_LOG_NAME'));
        // Set handler
        $logger->pushHandler($handler);

        return $logger;
    }
}
