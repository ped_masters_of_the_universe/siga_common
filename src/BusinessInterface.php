<?php

namespace Avantis\Common;

interface BusinessInterface
{

    public function setRepository($repository);

    public function getWarnings();

    public function setWarnings(array $warnings);

    public function isValid($process = null);

    public function notify($processs);

}