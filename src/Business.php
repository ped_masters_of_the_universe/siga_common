<?php

namespace Avantis\Common;

class Business implements BusinessInterface
{

    protected $_warnings;
    protected $_repository;


    public function setRepository($repository)
    {
        $this->_repository = $repository;
    }


    public function getWarnings()
    {
        return $this->_warnings;
    }


    public function setWarnings(array $warnings)
    {
        $this->_warnings = $warnings;
    }


    public function isValid($process = null)
    {
        return true;
    }


    public function notify($process = null)
    {

    }

}