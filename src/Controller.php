<?php

namespace Avantis\Common;

use Avantis\QueryParse\JWT;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    /**
     * @var Repository $_repository
     */
    protected $_repository;
    /**
     * @var Business $_business
     */
    protected $_business;
    protected $_URL;


    /**
     * Create a new controller instance.
     * @param Request $request
     * @param $repository
     * @param $business
     */
    public function __construct(Request $request, $repository, $business)
    {
        $this->_URL = $request->url();

        $data = $request->all();
        $data['row_user_uuid'] = JWT::extractValue($request->headers->get('token'), 'usuario_uuid');
        $data['row_remote_address'] = $request->ip();

        $data = Helpers::convertEmptyStringsToNull($data);

        $this->_repository = $repository;
        $this->_repository->setData($data);

        $this->_business = $business;
        $this->_business->setRepository($repository);
    }

    /**
     * @throws ExceptionResponse
     */
    protected function repositoryHasItem()
    {
        if (!$this->_repository->hasItem()) {
            throw new ExceptionResponse(['message' => 'Not Found'], 404);
        }
    }

    /**
     * @throws ExceptionResponse
     */
    protected function repositoryIsInTrash()
    {
        if ($this->_repository->isInTrash()) {
            throw new ExceptionResponse(preg_replace(
                '/\/[\d]+$/',
                '/trashed/' . $this->_repository->getItemID(), $this->_URL),
                301);
        }
    }

    /**
     * @throws ExceptionResponse
     */
    protected function repositoryIsNotInTrash()
    {
        if (!$this->_repository->isInTrash()) {
            throw new ExceptionResponse(preg_replace(
                '/\/trashed\/[\d]+$/',
                '/'.$this -> _repository->getItemID(), $this -> _URL),
                301
            );
        }
    }

    /**
     * @throws ExceptionResponse
     */
    protected function repositoryErrors()
    {
        throw new ExceptionResponse(['message' => $this->_repository->getErrors()], 422);
    }


    /**
     * 
     * @param BusinessProcess $processs
     * @throws ExceptionResponse
     */
    protected function businessIsValid($processs)
    {
        if (!$this->_business->isValid($processs))
            throw new ExceptionResponse(['message' => $this->_business->getWarnings()], 422);
    }

    /**
     * @param mixed $valor
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function response($valor, $code = 200)
    {
        return Helpers::jsonResponse($valor, $code);
    }

    /**
     *
     * @return int
     */
    private function uniqueInTrashId()
    {
        if (isset($this->_repository->_model->uniqueTrash) && count($this->_repository->_model->uniqueTrash) > 0) {
            $where = array_map(function ($valor) {
                return [$valor, '=', $this->_repository->getData()[$valor]];
            }, $this->_repository->_model->uniqueTrash);
            $where[] = ['row_flag', '=', true];
            $model = $this->_repository->_model->where($where)->first();
            $pk = $this->_repository->_model->getPrimaryKey();
            return !empty($model) && !empty($model[$pk]) ? $model[$pk] : 0;
        }

        return 0;
    }

    /**
     * Displays a list of resources.
     * @return mixed
     */
    protected function _index()
    {
        return $this->_repository->getCollection();
    }

    /**
     * Displays a list of resources.
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $dados = $this->_index();
        return $this->response($dados, 200);
    }

    /**
     * Displays the specified resource.
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model
     * @throws ExceptionResponse
     */
    protected function _show($id)
    {
        $this->_repository->setItemID($id);

        $this->repositoryHasItem();
        $this->repositoryIsInTrash();

        return $this->_repository->getItem();
    }

    /**
     * Displays the specified resource.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ExceptionResponse
     */
    public function show($id)
    {
        $item = $this->_show($id);

        return $this->response($item, 200);
    }

    /**
     * Stores a new resource.
     * @return \Illuminate\Database\Eloquent\Model
     * @throws ExceptionResponse
     */
    protected function _store()
    {
        $this->businessIsValid(BusinessProcess::STORE);

        if (!$this->_repository->insert()) {
            $this->repositoryErrors();
        }
        $this->_business->notify();
        return $this->_repository->getItem();
    }

    /**
     * Stores a new resource.
     * @return \Illuminate\Http\JsonResponse
     * @throws ExceptionResponse
     */
    public function store()
    {
        $idTrash = $this->uniqueInTrashId();
        if ($idTrash > 0) {
            try {
                DB::beginTransaction();
                $this->_repository->setItemID($idTrash);
                if (!$this->_repository->trashRestore()) {
                    $this->repositoryErrors();
                }
                $this->_patch($idTrash);
                DB::commit();
                return $this->response($this->_repository->getItem(), 200);
            }catch (\Exception $ex) {
                DB::rollBack();
                throw $ex;
            }
        }

        $item = $this->_store();
        return $this->response($item, 201);
    }

    /**
     * Update the specified resource.
     * @param  int $id
     * @return \Illuminate\Database\Eloquent\Model
     * @throws ExceptionResponse
     */
    protected function _patch($id)
    {
        $this->_repository->setItemID($id);

        $this->repositoryHasItem();
        $this->repositoryIsInTrash();
        $this->businessIsValid(BusinessProcess::PATCH);

        if (!$this->_repository->patch()) {
            $this->repositoryErrors();
        }
        $this->_business->notify();
        return $this->_repository->getItem();
    }

    /**
     * Update the specified resource.
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ExceptionResponse
     */
    public function patch($id)
    {
        $item = $this->_patch($id);
        return $this->response($item, 200);
    }

    /**
     * Update the specified resource.
     * @param  int $id
     * @return \Illuminate\Database\Eloquent\Model
     * @throws ExceptionResponse
     */
    protected function _update($id)
    {
        $this->_repository->setItemID($id);

        $this->repositoryHasItem();
        $this->repositoryIsInTrash();
        $this->businessIsValid(BusinessProcess::UPDATE);

        if (!$this->_repository->update()) {
            $this->repositoryErrors();
        }
        $this->_business->notify();
        return $this->_repository->getItem();
    }

    /**
     * Update the specified resource.
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ExceptionResponse
     */
    public function update($id)
    {
        $item = $this->_update($id);
        return $this->response($item, 200);
    }

    /**
     * Delete the specified resource.
     * @param  int $id
     * @return bool
     * @throws ExceptionResponse
     */
    protected function _destroy($id)
    {
        $this->_repository->setItemID($id);

        $this->repositoryHasItem();
        $this->repositoryIsInTrash();
        $this->businessIsValid(BusinessProcess::DESTROY);

        if (!$this->_repository->destroy()) {
            $this->repositoryErrors();
        }
        $this->_business->notify();
        return true;
    }

    /**
     * Delete the specified resource.
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ExceptionResponse
     */
    public function destroy($id)
    {
        $this->_destroy($id);
        return $this->response(null, 200);
    }

    /**
     * Displays resources in the trash.
     * @return mixed
     */
    protected function _trashIndex()
    {
        return $this->_repository->getCollection(true);
    }

    /**
     * Displays resources in the trash.
     * @return \Illuminate\Http\JsonResponse
     */
    public function trashIndex()
    {
        $dados = $this->_trashIndex();
        return $this->response($dados, 200);
    }

    /**
     * Displays the specified resource in the trash.
     * @param  int $id
     * @return \Illuminate\Database\Eloquent\Model
     * @throws ExceptionResponse
     */
    protected function _trashShow($id)
    {
        $this->_repository->setItemID($id);

        $this->repositoryHasItem();
        $this->repositoryIsNotInTrash();

        return $this->_repository->getItem();
    }

    /**
     * Displays the specified resource in the trash.
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ExceptionResponse
     */
    public function trashShow($id)
    {
        $item = $this->_trashShow($id);
        return $this->response($item, 200);
    }

    /**
     * Restores the specified resource in the trash.
     * @param  int $id
     * @return \Illuminate\Database\Eloquent\Model
     * @throws ExceptionResponse
     */
    protected function _trashRestore($id)
    {
        $this->_repository->setItemID($id);

        $this->repositoryHasItem();
        $this->repositoryIsNotInTrash();
        $this->businessIsValid(BusinessProcess::TRASH_RESTORE);

        if (!$this->_repository->trashRestore()) {
            $this->repositoryErrors();
        }
        $this->_business->notify();
        return $this->_repository->getItem();
    }

    /**
     * Restores the specified resource in the trash.
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws ExceptionResponse
     */
    public function trashRestore($id)
    {
        $this->_trashRestore($id);
        return $this->response(null, 200);
    }















}




