<?php

namespace Avantis\Common;

use Illuminate\Support\Facades\Validator;

class Helpers
{

    /**
     * Return to view in json format
     * @param String|array $content
     * @param Int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    public static function jsonResponse($content, $statusCode)
    {
        return response()->json($content, $statusCode, ['Content-Type' => 'application/json']);
    }

    /**
     * Apply validation according to the rules
     * @param array $array
     * @return array
     */
    public static function convertEmptyStringsToNull($array)
    {
        if (is_array($array) && !empty($array)) {
            $new_array = [];
            foreach ($array as $key => $value) {
                $new_array[$key] = is_string($value) && $value === '' ? null : $value;
            }
            return $new_array;
        }
        return $array;
    }

    /**
     * Apply validation according to the rules
     * @param \Illuminate\Http\Request $request
     * @param array $rules
     * @return array
     */
    public static function validateRequestBody($request, $rules)
    {
        $validator = Validator::make(
            $request->all(),
            $rules,
            [
                'exists' => ':attribute não corresponde a nenhuma entidade cadastrada',
                'unique' => ':attribute informado encontra-se em uso',
                'required' => ':attribute deve ser informado',
                'max' => ':attribute ultrapassa o limite de caracteres',
                'min' => ':attribute minimo de caracteres contemplado',
                'numeric' => ':attribute deve ser um valor númerico',
                'email' => ':attribute não está em formato de email válido',
                'regex' => ':attribute não está em formato válido'
            ]
        );

        return $validator->errors()->all();
    }

    /**
     * Apply validation according to the rules
     * @param \Illuminate\Http\Request $request
     * @param array $rules
     * @return array
     */
    public static function validateRequestHeader($request, $rules)
    {
        $validator = Validator::make(
            $request->headers->all(),
            $rules,
            [
                'exists' => ':attribute não corresponde a nenhuma entidade cadastrada',
                'unique' => ':attribute informado encontra-se em uso',
                'required' => ':attribute deve ser informado',
                'max' => ':attribute ultrapassa o limite de caracteres',
                'min' => ':attribute minimo de caracteres contemplado',
                'numeric' => ':attribute deve ser um valor númerico',
                'email' => ':attribute não está em formato de email válido',
                'regex' => ':attribute não está em formato válido'
            ]
        );

        return $validator->errors()->all();
    }

}