<?php
namespace Avantis\Common;

use Avantis\QueryParse\JWT;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class RelationshipController extends BaseController
{

    /**
     *
     * @var Repository|RelationshipRepository $_repository
     */
    protected $_repository;

    /**
     *
     * @var Business $_business
     */
    protected $_business;

    /**
     *
     * @var string
     */
    protected $_URL;

    /**
     * Create a new relationship controller instance.
     *
     * @param Request $request
     * @param
     *            $repository
     * @param
     *            $business
     */
    public function __construct(Request $request, $repository, $business)
    {
        $this->_URL = $request->url();

        $data = $request->all();
        $data['row_user_uuid'] = JWT::extractValue($request->headers->get('token'), 'usuario_uuid');
        $data['row_remote_address'] = $request->ip();

        $data = Helpers::convertEmptyStringsToNull($data);

        $this->_repository = $repository;
        $this->_repository->setData($data);

        $this->_business = $business;
        $this->_business->setRepository($repository);
    }

    /**
     * Displays a list of relationship resources.
     *
     * @param int $leftItemID
     * @return String
     */
    public function index($leftItemID)
    {
        $this->_repository->setLeftItemID($leftItemID);

        if (! $this->_repository->hasLeftItem())
            return Helpers::jsonResponse([
                'message' => 'Not Found'
            ], 404);

        return Helpers::jsonResponse($this->_repository->getCollection($leftItemID), 200);
    }

    /**
     * Displays the specified relationship resource.
     *
     * @param int $leftItemID
     * @param int $rightItemID
     * @return String
     */
    public function show($leftItemID, $rightItemID)
    {
        $this->_repository->setItemIDByParentIDs($leftItemID, $rightItemID);

        if (! $this->_repository->hasItem())
            return Helpers::jsonResponse([
                'message' => 'Not Found'
            ], 404);

        return Helpers::jsonResponse($this->_repository->getItem(), 200);
    }

    /**
     * Stores a new relationship resource.
     *
     * @param int $leftItemID
     * @return String
     */
    public function store($leftItemID)
    {
        $this->_repository->setLeftItemID($leftItemID);

        if (! $this->_repository->hasLeftItem())
            return Helpers::jsonResponse([
                'message' => 'Not Found'
            ], 404);

        if (! $this->_business->isValid(BusinessProcess::STORE))
            return Helpers::jsonResponse([
                'message' => $this->_business->getWarnings()
            ], 422);

        if ($this->_repository->insert()) {
            $this->_business->notify();
            return Helpers::jsonResponse($this->_repository->getItem(), 201);
        } else {
            return Helpers::jsonResponse([
                'message' => $this->_repository->getErrors()
            ], 422);
        }
    }

    /**
     * Update the specified resource.
     *
     * @param int $leftItemID
     * @param int $rightItemID
     * @return String
     */
    public function patch($leftItemID, $rightItemID)
    {
        $this->_repository->setItemIDByParentIDs($leftItemID, $rightItemID);

        if (! $this->_repository->hasItem())
            return Helpers::jsonResponse([
                'message' => 'Not Found'
            ], 404);

        if (! $this->_business->isValid(BusinessProcess::PATCH))
            return Helpers::jsonResponse([
                'message' => $this->_business->getWarnings()
            ], 422);

        if ($this->_repository->patch()) {
            $this->_business->notify();
            return Helpers::jsonResponse($this->_repository->getItem(), 200);
        } else {
            return Helpers::jsonResponse([
                'message' => $this->_repository->getErrors()
            ], 422);
        }
    }

    /**
     * Update the specified relationship resource.
     *
     * @param int $leftItemID
     * @param int $rightItemID
     * @return String
     */
    public function update($leftItemID, $rightItemID)
    {
        $this->_repository->setItemIDByParentIDs($leftItemID, $rightItemID);

        if (! $this->_repository->hasItem())
            return Helpers::jsonResponse([
                'message' => 'Not Found'
            ], 404);

        if (! $this->_business->isValid(BusinessProcess::UPDATE))
            return Helpers::jsonResponse([
                'message' => $this->_business->getWarnings()
            ], 422);

        if ($this->_repository->update()) {
            $this->_business->notify();
            return Helpers::jsonResponse($this->_repository->getItem(), 200);
        } else {
            return Helpers::jsonResponse([
                'message' => $this->_repository->getErrors()
            ], 422);
        }
    }

    /**
     * Delete the specified relationship resource.
     *
     * @param int $leftItemID
     * @param int $rightItemID
     * @return String
     */
    public function destroy($leftItemID, $rightItemID)
    {
        $this->_repository->setItemIDByParentIDs($leftItemID, $rightItemID);

        if (! $this->_repository->hasItem())
            return Helpers::jsonResponse([
                'message' => 'Not Found'
            ], 404);

        if (! $this->_business->isValid(BusinessProcess::DESTROY))
            return Helpers::jsonResponse([
                'message' => $this->_business->getWarnings()
            ], 422);

        if ($this->_repository->destroy()) {
            $this->_business->notify();
            return Helpers::jsonResponse(null, 200);
        } else {
            return Helpers::jsonResponse([
                'message' => $this->_repository->getErrors()
            ], 422);
        }
    }
}




