<?php

namespace Avantis\Common;

interface RelationshipModelInterface
{

    public function updateRules();

    public function insertRules();

    public function getPrimaryKey();

    public function getLeftForeignKey();

    public function getRightForeignKey();

}