<?php

namespace Avantis\Common;

use Illuminate\Http\Request;
use Avantis\QueryParse\LCP;

class Authorization
{

    /**
     * Verify permission in microservice security
     *
     * @param  \Illuminate\Http\Request $request
     * @return boolean
     */
    public static function check(Request $request)
    {
        $jwt = @$request->headers->get('token');
        $app_uuid = @$request->headers->get('app-uuid');
        $client_id = @$request->headers->get('client-id');
        $client_secret = @$request->headers->get('client-secret');

        if ((!$jwt && !$app_uuid) && (!$client_id && !$client_secret))
            return false;

        if (empty($client_id))
            $client_id = env('CLIENT_ID');

        if (empty($client_secret))
            $client_secret = env('CLIENT_SECRET');

        $LCP = new LCP(
            env('METHOD_AUTORIZAR'),
            env('ENDPOINT_AUTORIZAR'),
            [
                'token' => $jwt,
                'app-uuid' => $app_uuid,
                'client-id' => $client_id,
                'client-secret' => $client_secret,
            ]
        );

        $LCP->setData([
            'method' => $request->method(),
            'path' => $request->path(),
            'ip' => $request->ip()
        ]);

        $LCP->request();

        try {
            $status_code_response = $LCP->getReponse();
        } catch (\Exception $exception) {
            $status_code_response = false;
        }

        return $status_code_response;
    }

}