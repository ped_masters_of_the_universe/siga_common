<?php

namespace Avantis\Common;

abstract class BusinessProcess {
    const STORE = 'store';
    const PATCH = 'patch';
    const UPDATE = 'update';
    const DESTROY = 'destroy';
    const TRASH_RESTORE = 'trashRestore';
}