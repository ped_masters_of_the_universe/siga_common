<?php

namespace Avantis\Common;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class RelationshipRepository implements RelationshipRepositoryInterface
{

    /**
     * @var Model|ModelInterface|RelationshipModelInterface $_model
     */
    public $_model;
    /**
     * @var Model|ModelInterface|RelationshipModelInterface $_leftModel
     */
    public $_leftModel;
    /**
     * @var Model|ModelInterface|RelationshipModelInterface $_rightModel
     */
    public $_rightModel;
    protected $_data;
    protected $_errors;

    protected $_validationMessages = [
        'integer' => 'deve ser um valor numerico',
        'string' => 'valor deve ser um texto',
        'exists' => 'não corresponde a nenhuma entidade cadastrada',
        'unique' => 'informado encontra-se em uso',
        'required' => 'deve ser informado',
        'max' => 'ultrapassa o limite de caracteres',
        'min' => 'minimo de caracteres contemplado',
        'numeric' => 'deve ser um valor númerico',
        'email' => 'não está em formato de email válido',
        'regex' => 'não está em formato válido',
        'filled' => 'informado esta vazio',
        'date_format' => 'não está no formato válido',
        'date' => 'não é uma data válida',
    ];

    /**
     * RelationshipRepository constructor.
     * @param Model|ModelInterface|RelationshipModelInterface $model
     * @param Model|ModelInterface|RelationshipModelInterface|null $leftModel
     * @param Model|ModelInterface|RelationshipModelInterface|null $rightModel
     */
    public function __construct($model, $leftModel = null, $rightModel = null)
    {
        $this->_model = $model;
        $this->_leftModel = $leftModel;
        $this->_rightModel = $rightModel;
    }


    /**
     * @param array $data
     * */
    public function setData($data)
    {
        $this->_data = $data;
    }


    /**
     * @param int $id
     * */
    public function setItemID($id)
    {
        $this->_model = $this->_model->find($id);
    }


    /**
     * @param int $leftItemID
     * @param int $rightItemID
     * */
    public function setItemIDByParentIDs($leftItemID, $rightItemID)
    {
        $this->_model = $this->_model
            ->where([$this->_model->getLeftForeignKey() => $leftItemID])
            ->where([$this->_model->getRightForeignKey() => $rightItemID])
            ->first();
    }


    /**
     * @return int
     * */
    public function getItemID()
    {
        return $this->_model[$this->_model->getPrimaryKey()];
    }


    /**
     * @return boolean
     * */
    public function hasItem()
    {
        return ($this->_model) ? true : false;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Model
     * */
    public function getItem()
    {
        return $this->_model->find($this->getItemID());
    }


    /**
     * @param int $id
     * */
    public function setLeftItemID($id)
    {
        $this->_leftModel = $this->_leftModel->find($id);
    }


    /**
     * @return int
     * */
    public function getLeftItemID()
    {
        return $this->_leftModel[$this->_leftModel->getPrimaryKey()];
    }


    /**
     * @return boolean
     * */
    public function hasLeftItem()
    {
        return ($this->_leftModel) ? true : false;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Model
     * */
    public function getLeftItem()
    {
        return $this->_leftModel->find($this->getItemID());
    }


    /**
     * @param int $id
     * */
    public function setRightItemID($id)
    {
        $this->_rightModel = $this->_rightModel->find($id);
    }


    /**
     * @return int
     * */
    public function getRighttemID()
    {
        return $this->_rightModel[$this->_rightModel->getPrimaryKey()];
    }


    /**
     * @return boolean
     * */
    public function hasRightItem()
    {
        return ($this->_rightModel) ? true : false;
    }


    /**
     * @return \Illuminate\Database\Eloquent\Model
     * */
    public function getRightItem()
    {
        return $this->_rightModel->find($this->getItemID());
    }


    /**
     * @param array $rules
     * @return array|false
     */
    public function validate($rules)
    {
        $validator = Validator::make(
            $this->_data,
            $rules,
            $this->_validationMessages
        );

        $messages = $validator->errors()->messages();

        if (!empty($messages)) {
            $results = [];
            foreach ($messages as $key => $value) {
                $results[$key] = $value[0];
            }

            return $results;
        }

        return false;
    }


    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->_errors;
    }


    /**
     * @param int $leftItemID
     * @return array
     */
    public function getCollection($leftItemID)
    {
        if (!preg_match('/^[1-9][0-9]*$/', @$this->_data['items_per_page']))
            $paginate = env('ITEMS_PER_PAGE');
        else
            $paginate = $this->_data['items_per_page'];

        return $this->_model
            ->search($this->_data)
            ->where([$this->_model->getLeftForeignKey() => $leftItemID])
            ->paginate($paginate);
    }


    /**
     * @return boolean
     */
    public function insert()
    {
        unset($this->_data[$this->_model->getPrimaryKey()]);

        $this->_model->fill($this->_data);
        $this->_errors = $this->validate($this->_model->insertRules());

        if ($this->_errors == null) {

            try {
                $this->_model->save();
                return true;
            } catch (\Exception $e) {
                $this->_errors = $e->getMessage();
                return false;
            }

        } else {
            return false;
        }
    }


    /**
     * @return boolean
     */
    public function patch()
    {
        unset($this->_data[$this->_model->getPrimaryKey()]);
        unset($this->_data[$this->_model->getLeftForeignKey()]);
        unset($this->_data[$this->_model->getRightForeignKey()]);

        $validation = array_intersect_key($this->_model->updateRules(), $this->_data);

        $this->_model->fill($this->_data);
        $this->_errors = $this->validate($validation);

        if ($this->_errors == null) {

            try {
                $this->_model->save();
                return true;
            } catch (\Exception $e) {
                $this->_errors = $e->getMessage();
                return false;
            }

        } else {
            return false;
        }
    }


    /**
     * @return boolean
     */
    public function update()
    {
        unset($this->_data[$this->_model->getPrimaryKey()]);
        unset($this->_data[$this->_model->getLeftForeignKey()]);
        unset($this->_data[$this->_model->getRightForeignKey()]);

        $this->_model->fill($this->_data);
        $this->_errors = $this->validate($this->_model->updateRules());

        if ($this->_errors == null) {

            try {
                $this->_model->save();
                return true;
            } catch (\Exception $e) {
                $this->_errors = $e->getMessage();
                return false;
            }

        } else {
            return false;
        }
    }


    /**
     * @return boolean
     */
    public function destroy()
    {
        try {
            $this->_model->delete();
            return true;
        } catch (\Exception $e) {
            $this->_errors = $e->getMessage();
            return false;
        }
    }

    /**
     * @param bool $select
     * @param $array
     * @param bool $groupBy
     * @param bool $orderBy
     * @return array
     */
    public function query($select = false, $array = [], $groupBy = false, $orderBy = false)
    {
        if (empty($select))
            $select = '*';

        if (!empty($groupBy)) {
            $query = $this->_model
                ->select($select)
                ->where($array)
                ->groupBy($groupBy);

        } else {
            $query = $this->_model
                ->select($select)
                ->where($array);
        }

        if (!empty($orderBy) && is_array($orderBy)) {
            foreach ($orderBy as $column => $direction) {
                $query->orderBy($column, $direction);
            }
        }

        return $query
            ->get()
            ->toArray();
    }

}