<?php

namespace Avantis\Common;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Repository implements RepositoryInterface
{

    /**
     * @var Model|ModelInterface $_model ;
     */
    public $_model;
    protected $_data;
    protected $_errors;

    protected $_validationMessages = [
        'integer' => 'deve ser um valor numerico',
        'string' => 'valor deve ser um texto',
        'exists' => 'não corresponde a nenhuma entidade cadastrada',
        'unique' => 'informado encontra-se em uso',
        'required' => 'deve ser informado',
        'max' => 'ultrapassa o limite de caracteres',
        'min' => 'minimo de caracteres contemplado',
        'numeric' => 'deve ser um valor númerico',
        'email' => 'não está em formato de email válido',
        'regex' => 'não está em formato válido',
        'filled' => 'informado esta vazio',
        'date_format' => 'não está no formato válido',
        'date' => 'não é uma data válida',
    ];


    public function __construct($model)
    {
        $this->_model = $model;
    }


    /**
     * @param array
     * */
    public function setData($data)
    {
        $this->_data = $data;
    }

    /**
     * @return array
     * */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * @param Int $id
     * */
    public function setItemID($id)
    {
        $this->_model = $this->_model->find($id);
    }


    /**
     * @return Int
     * */
    public function getItemID()
    {
        return $this->_model[$this->_model->getPrimaryKey()];
    }


    /**
     * @return boolean
     */
    public function hasItem()
    {
        return ($this->_model) ? true : false;
    }


    /**
     * @return boolean
     */
    public function isInTrash()
    {
        return $this->_model['row_flag'];
    }


    /**
     * @param array $rules
     * @return array|false
     */
    public function validate($rules)
    {
        $validator = Validator::make(
            $this->_data,
            $rules,
            $this->_validationMessages
        );

        $messages = $validator->errors()->messages();

        if (!empty($messages)) {
            $results = [];
            foreach ($messages as $key => $value) {
                $results[$key] = $value[0];
            }

            return $results;
        } else {
            return false;
        }
    }


    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->_errors;
    }


    /**
     * @param boolean $row_flag
     * @return mixed
     */
    public function getCollection($row_flag = false)
    {
        return $this->_model->search($this->_data)->where(['row_flag' => $row_flag])->paginate($this->getPagination());
    }

    protected function getPagination() {
        if (!preg_match('/^[1-9][0-9]*$/', @$this->_data['items_per_page']))
            return env('ITEMS_PER_PAGE');
        else
            return $this->_data['items_per_page'];
    }


    /**
     * @return \Illuminate\Database\Eloquent\Model
     * */
    public function getItem()
    {
        return $this->_model->find($this->getItemID());
    }


    /**
     * @return boolean
     */
    public function insert()
    {
        unset($this->_data[$this->_model->getPrimaryKey()]);

        $this->_model->fill($this->_data);
        $this->_errors = $this->validate($this->_model->insertRules());

        if ($this->_errors == null) {

            try {
                $this->_model->save();
                return true;
            } catch (\Exception $e) {
                $this->_errors = $e->getMessage();
                return false;
            }

        } else {
            return false;
        }
    }


    /**
     * @return boolean
     */
    public function patch()
    {
        unset($this->_data[$this->_model->getPrimaryKey()]);

        $validation = array_intersect_key($this->_model->updateRules(), $this->_data);

        $this->_model->fill($this->_data);
        $this->_errors = $this->validate($validation);

        if ($this->_errors == null) {

            try {
                $this->_model->save();
                return true;
            } catch (\Exception $e) {
                $this->_errors = $e->getMessage();
                return false;
            }

        } else {
            return false;
        }
    }


    /**
     * @return boolean
     */
    public function update()
    {
        unset($this->_data[$this->_model->getPrimaryKey()]);

        $this->_model->fill($this->_data);
        $this->_errors = $this->validate($this->_model->updateRules());

        if ($this->_errors == null) {

            try {
                $this->_model->save();
                return true;
            } catch (\Exception $e) {
                $this->_errors = $e->getMessage();
                return false;
            }

        } else {
            return false;
        }
    }


    /**
     * @return boolean
     */
    public function destroy()
    {
        try {
            $this->_model->update(['row_flag' => true]);
            return true;
        } catch (\Exception $e) {
            $this->_errors = $e->getMessage();
            return false;
        }
    }


    /**
     * @return boolean
     */
    public function trashRestore()
    {
        try {
            $this->_model->update(['row_flag' => false]);
            return true;
        } catch (\Exception $e) {
            $this->_errors = $e->getMessage();
            return false;
        }
    }

    /**
     * @param bool $select
     * @param $array
     * @param bool $groupBy
     * @param bool $orderBy
     * @return array
     */
    public function query($select = false, $array = [], $groupBy = false, $orderBy = false)
    {
        if (empty($select))
            $select = '*';

        if (!empty($groupBy)) {
            $query = $this->_model
                ->select($select)
                ->where($array)
                ->groupBy($groupBy);

        } else {
            $query = $this->_model
                ->select($select)
                ->where($array);
        }

        if (!empty($orderBy) && is_array($orderBy)) {
            foreach ($orderBy as $column => $direction) {
                $query->orderBy($column, $direction);
            }
        }

        return $query
            ->get()
            ->toArray();
    }

}