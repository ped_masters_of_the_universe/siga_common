<?php

namespace Avantis\Common;

interface RepositoryInterface
{

    public function setData($data);

    public function setItemID($id);

    public function getItemID();

    public function hasItem();

    public function isInTrash();

    public function validate($rules);

    public function getErrors();

    public function getCollection($row_flag);

    public function getItem();

    public function insert();

    public function patch();

    public function update();

    public function destroy();

    public function trashRestore();

    public function query($select, $array, $groupBy, $orderBy);

}