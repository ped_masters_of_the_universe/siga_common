<?php
/**
 * Created by PhpStorm.
 * User: vagner
 * Date: 18/10/18
 * Time: 09:48
 */

namespace Avantis\Common;


class ExceptionResponse extends \Exception
{
    /**
     * @var int
     */
    private $codeResponse = 500;

    /**
     * @var mixed
     */
    private $messageResponse;

    /**
     * ExceptionResponse constructor.
     * @param mixed $messageResponse
     * @param string $message
     * @param int $codeResponse
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct($messageResponse, $codeResponse, $message = "", $code = 0, \Exception $previous = null)
    {
        $this->codeResponse = $codeResponse;
        $this->messageResponse = $messageResponse;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return int
     */
    public function getCodeResponse()
    {
        return $this->codeResponse;
    }

    /**
     * @return mixed
     */
    public function getMessageResponse()
    {
        return $this->messageResponse;
    }

}