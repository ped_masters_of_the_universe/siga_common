<?php

namespace Avantis\Common;

interface ModelInterface
{

    public function updateRules();

    public function insertRules();

    public function getPrimaryKey();

}