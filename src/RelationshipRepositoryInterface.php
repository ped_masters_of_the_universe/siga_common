<?php

namespace Avantis\Common;

interface RelationshipRepositoryInterface
{

    public function setData($data);

    public function setItemID($id);

    public function setItemIDByParentIDs($leftItemID, $rightItemID);

    public function getItemID();

    public function hasItem();

    public function getItem();

    public function setLeftItemID($id);

    public function getLeftItemID();

    public function hasLeftItem();

    public function getLeftItem();

    public function setRightItemID($id);

    public function getRighttemID();

    public function hasRightItem();

    public function getRightItem();

    public function validate($rules);

    public function getErrors();

    public function getCollection($leftItemID);

    public function insert();

    public function patch();

    public function update();

    public function destroy();

    public function query($select, $array, $groupBy, $orderBy);

}